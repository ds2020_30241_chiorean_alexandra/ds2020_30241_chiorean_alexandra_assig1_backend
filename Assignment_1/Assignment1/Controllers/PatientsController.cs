﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Assignment1.DAL;
using Assignment1.Models.Entities;
using Assignment1.Service;
using Microsoft.AspNetCore.Authorization;
using Assignment1.Models.Entities.Authentication;
using Assignment1.Models.Entities.EntitiesMapper;

namespace Assignment1.Controllers
{
   
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "Doctor,Patient,Caregiver")]
    public class PatientsController : ControllerBase
    {
        private readonly IPatientService _patientService;

        public PatientsController(IPatientService patientService)
        {
            this._patientService = patientService;
        }

        // GET: api/Patients
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Patient>>> GetPacients()
        {
            return Ok(this._patientService.GetPatients());
        }

        // GET: api/Patients/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Patient>> GetPatientsById(int id)
        {

            return Ok(this._patientService.GetPatientsById(id));

        }
        [HttpGet("userId/{id}")]
        public async Task<ActionResult<Patient>> GetPatientsByUserId(string id)
        {

            return Ok(this._patientService.GetPatientByUserId(id));

        }
        // GET: api/Patients/5
        [HttpGet("doctorId/{doctorId}")]
        public async Task<ActionResult<Patient>> GetPatientByDoctorId(int doctorId)
        {

            return Ok(this._patientService.GetPatientsByDoctor(doctorId));

        }
        [HttpGet("caregiverId/{caregiverId}")]
        public async Task<ActionResult<PatientMapper>> GetPatientByCaregiverId(int caregiverId)
        {
            Console.WriteLine(caregiverId);
            return Ok(this._patientService.GetPatientsByCaregiver(caregiverId));

        }

        // PUT: api/Patients/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutPatient(int id, Patient patient)
        {
            this._patientService.UpdatePatient(patient);
            return Ok();
        }

        // POST: api/Patients
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost]
        public async Task<ActionResult<Patient>> PostPatient(Patient patient)
        {

            this._patientService.AddPatient(patient);
            return Ok();
           
        }

        // DELETE: api/Patients/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<Patient>> DeletePatient(int id)
        {
            this._patientService.DeletePatient(id);
            return Ok();

        }

        private bool PatientExists(int id)
        {
            return false;
        }
    }
}
