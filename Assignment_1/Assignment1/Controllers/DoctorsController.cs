﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Assignment1.DAL;
using Assignment1.Models.Entities;
using Assignment1.Service;
using Microsoft.AspNetCore.Authorization;

namespace Assignment1.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "Doctor")]
    public class DoctorsController : ControllerBase
    {
        private readonly IDoctorService _doctoreService;

        public DoctorsController(IDoctorService doctorService)
        {
            _doctoreService = doctorService;
        }

        // GET: api/Doctors
        
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Doctor>>> GetDoctors()
        {
            return Ok();
        }

        // GET: api/Doctors/5
        [HttpGet("userId/{id}")]
        public async Task<ActionResult<Doctor>> GetDoctor(string id)
        {
            return Ok(_doctoreService.GetDoctorByUserId(id));
        }

        // PUT: api/Doctors/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutDoctor(int id, Doctor doctor)
        {
            
            return NoContent();
        }

        // POST: api/Doctors
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost]
        public async Task<ActionResult<Doctor>> PostDoctor(Doctor doctor)
        {

            this._doctoreService.AddDoctor(doctor);
            return CreatedAtAction("GetDoctor", new { id = doctor.Id }, doctor);
        }

        // DELETE: api/Doctors/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<Doctor>> DeleteDoctor(int id)
        {
            return Ok();
        }

        private bool DoctorExists(int id)
        {
            return false;
        }
    }
}
