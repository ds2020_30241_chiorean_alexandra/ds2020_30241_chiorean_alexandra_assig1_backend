﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Assignment1.DAL;
using Assignment1.Models.Entities;
using Assignment1.Service;
using Microsoft.AspNetCore.Authorization;

namespace Assignment1.Controllers
{
   
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "Doctor")]
    public class MedicationsController : ControllerBase
    {
        private readonly IMedicationService _medService;

        public MedicationsController(IMedicationService medicationService)
        {
            this._medService = medicationService;
        }

        // GET: api/Medications
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Medication>>> GetMedications()
        {
            return Ok(this._medService.GetMedication());

        }

        // GET: api/Medications/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Medication>> GetMedication(int id)
        {
            return Ok(_medService.GetMedicationById(id));
        }

        // PUT: api/Medications/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutMedication(int id, Medication medication)
        {
            if (id != medication.Id)
            {
                return BadRequest();
            }

            this._medService.UpdateMedication(medication);
            return Ok();
        }

        // POST: api/Medications
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost]
        public async Task<ActionResult<Medication>> PostMedication(Medication medication)
        {
            this._medService.AddMedication(medication);
            return Ok();
        }

        // DELETE: api/Medications/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<Medication>> DeleteMedication(int id)
        {
            return Ok(this._medService.DeleteMedication(id));
            
        }

        
    }
}
