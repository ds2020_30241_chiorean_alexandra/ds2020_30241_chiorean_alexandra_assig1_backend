﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Assignment1.DAL;
using Assignment1.Models.Entities;
using Assignment1.Service;
using Microsoft.AspNetCore.Authorization;
using Assignment1.Models.Entities.EntitiesMapper;

namespace Assignment1.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "Doctor,Patient")]
    public class MedicationPlansController : ControllerBase
    {
        private readonly IMedicationPlanService _medicationPlanService;

        public MedicationPlansController(IMedicationPlanService medicationPlansService)
        {
            this._medicationPlanService = medicationPlansService;
        }

        // GET: api/MedicationPlans
        [HttpGet]
        public async Task<ActionResult<IEnumerable<MedicationPlan>>> GetMedicationPlans()
        {
            return Ok(this._medicationPlanService.GetPlan());
        }

        // GET: api/MedicationPlans/5
        [HttpGet("{id}")]
        public async Task<ActionResult<MedicationPlan>> GetMedicationPlan(int id)
        {
            return Ok(this._medicationPlanService.GetPlanById(id));
        }

        [HttpGet("patientId/{id}")]
        public async Task<ActionResult<MedicationPlanMapper>> GetMedicationPlanByPatientId(int id)
        {
            return Ok(this._medicationPlanService.GetMedicationPlansByPatient(id));
        }

        // PUT: api/MedicationPlans/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutMedicationPlan(int id, MedicationPlan medicationPlan)
        {
            return Ok(this._medicationPlanService.UpdatePlan(medicationPlan));
        }

        // POST: api/MedicationPlans
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost]
        public async Task<ActionResult<MedicationPlan>> PostMedicationPlan(MedicationPlan medicationPlan)
        {
            this._medicationPlanService.AddPlan(medicationPlan);

            return Ok();
        }

        // DELETE: api/MedicationPlans/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<MedicationPlan>> DeleteMedicationPlan(int id)
        {
            var medicationPlan = _medicationPlanService.GetPlanById(id);
            if (medicationPlan == null)
            {
                return NotFound();
            }

            this._medicationPlanService.DeletePlan(id);

            return Ok();
        }

        private bool MedicationPlanExists(int id)
        {
            return true;
        }
    }
}
