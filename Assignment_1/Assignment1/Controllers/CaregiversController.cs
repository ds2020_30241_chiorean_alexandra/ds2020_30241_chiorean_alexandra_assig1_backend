﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Assignment1.DAL;
using Assignment1.Models.Entities;
using Assignment1.Service;
using System.Runtime.InteropServices.WindowsRuntime;
using Microsoft.AspNetCore.Authorization;

namespace Assignment1.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "Doctor,Caregiver")]
    public class CaregiversController : ControllerBase
    {
        private readonly ICaregiverService _caregiversService;

        public CaregiversController(ICaregiverService caregiverService)
        {
            _caregiversService = caregiverService;
        }

        // GET: api/Caregivers
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Caregiver>>> GetCaregivers()
        {
            return Ok(this._caregiversService.GetCaregivers());
        }

        // GET: api/Caregivers/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Caregiver>> GetCaregiver(int id)
        {
            return Ok(this._caregiversService.GetCaregiversById(id));
        }
        [AllowAnonymous]
        [HttpGet("userId/{id}")]
        public async Task<ActionResult<Caregiver>> GetCaregiverByUserId(string id)
        {
            return Ok(this._caregiversService.GetCaregiverByUserId(id));
        }

        // PUT: api/Caregivers/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutCaregiver(int id, Caregiver caregiver)
        {

            if (id != caregiver.Id)
            {
                return BadRequest();
            }

            this._caregiversService.UpdateCaregiver(caregiver);
            return Ok();
        }

        // POST: api/Caregivers
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost]
        public async Task<ActionResult<Caregiver>> PostCaregiver(Caregiver caregiver)
        {
            this._caregiversService.AddCaregiver(caregiver);

            return CreatedAtAction("GetCaregiver", new { id = caregiver.Id }, caregiver);
        }

        // DELETE: api/Caregivers/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<Caregiver>> DeleteCaregiver(int id)
        {
            this._caregiversService.DeleteCaregiver(id);
            return Ok();
        }

        private bool CaregiverExists(int id)
        {
            return false;
        }
    }
}
