using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Assignment1.DAL;
using Assignment1.Hubs;
using Assignment1.Models;
using Assignment1.Models.Entities;
using Assignment1.Models.Entities.Authentication;
using Assignment1.Service;
using Assignment1.Service.MessageReceive;
using AutoMapper;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.IdentityModel.Tokens;

namespace Assignment1
{
    public class Startup
    {
        readonly string MyAllowSpecificOrigins = "_myAllowSpecificOrigins";
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddCors();
            services.AddControllers();
            /*var builder = new PostgreSqlConnectionStringBuilder("postgres://gggoqhfbgqxqxl:8ca66be4270f3140fae52e840c43a142e6d7278ec3cfab6952c9d5d0fdcf6be5@ec2-54-82-208-124.compute-1.amazonaws.com:5432/dfqvdan113ilgt")
              {
                Pooling = true,
                TrustServerCertificate = true,
                SslMode = SslMode.Require
            };*/
            var builder = new PostgreSqlConnectionStringBuilder(Environment.GetEnvironmentVariable("DATABASE_URL"));
            Console.WriteLine("DEBUG CS"+builder.ConnectionString);

            services.AddDbContext<ApplicationContext>(options =>
                options.UseNpgsql(builder.ConnectionString));



            /*services.AddDbContext<ApplicationContext>(options =>
               options.UseNpgsql(Configuration.GetConnectionString("DefaultConnection")));*/


            services.AddAutoMapper(typeof(Startup));
            services.AddIdentity<UserAccount, IdentityRole>(options =>

                {
                    options.User.RequireUniqueEmail = true;

                })
               .AddRoleManager<RoleManager<IdentityRole>>()
               .AddEntityFrameworkStores<ApplicationContext>()
               .AddDefaultTokenProviders();
            var jwtOptions = Configuration.GetSection("JWT_deploy").GetChildren().ToArray();
            services.AddAuthentication(options =>
            {
                options.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                options.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
                options.DefaultScheme = JwtBearerDefaults.AuthenticationScheme;
            })
             .AddJwtBearer(options =>
              {
                  options.SaveToken = true;
                  options.RequireHttpsMetadata = false;
                  options.TokenValidationParameters = new TokenValidationParameters()
                  {
                      ValidateIssuer = true,
                      ValidateAudience = true,
                      ValidAudience = jwtOptions[1].Value,
                      ValidIssuer = jwtOptions[2].Value,
                      IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(jwtOptions[0].Value))
                  };
              });
            services.AddScoped<IUnitOfWork, UnitOfWork>();
            services.AddScoped<IDoctorService, DoctorService>();
            services.AddScoped<IMedicationPlanService, MedicationPlanService>();
            services.AddScoped<IMedicationService, MedicationService>();
            services.AddScoped<IPatientService, PatientService>();
            services.AddScoped<ICaregiverService, CaregiversService>();
            services.AddScoped<IActivityService, ActivityService>();
            services.AddSignalR();
            services.AddHostedService<Receiver>();


        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseHttpsRedirection();


            app.UseRouting();

            app.UseCors(options => options.SetIsOriginAllowed(x => _ = true).AllowAnyMethod().AllowAnyHeader().AllowCredentials());

            app.UseAuthentication();
            app.UseAuthorization();
            

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllerRoute(
                   name: "default",
                   pattern: "api/{controller=Patients}/");
                endpoints.MapHub<NotificationHub>("/notification");
            });
        
        }
    }
}
