﻿using Assignment1.DAL;
using Assignment1.Models.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Assignment1.Service
{
    public class MedicationService:IMedicationService
    {
        private IUnitOfWork _unitOfWork;

        public MedicationService(IUnitOfWork unitOfWork)
        {
            this._unitOfWork = unitOfWork;
        }

        public bool AddMedication(Medication drug)
        {
            this._unitOfWork.MedicationRepo.Insert(drug);
            this._unitOfWork.Save();
            return true;
        }

        public IEnumerable<Medication> GetMedication()
        {
            Func<IQueryable<Medication>, IOrderedQueryable<Medication>> order = query => query.OrderBy(medication => medication.Id);
            return this._unitOfWork.MedicationRepo.Get(orderBy: order);

        }

        public IEnumerable<Medication> GetMedicationById(int id)
        {
            Expression<Func<Medication, bool>> query = med => med.Id == id;
            return this._unitOfWork.MedicationRepo.Get(query);

        }

        public bool UpdateMedication(Medication drug)
        {
            this._unitOfWork.MedicationRepo.Update(drug);
            this._unitOfWork.Save();
            return true;
        }

        public bool DeleteMedication(int id)
        {
            this._unitOfWork.MedicationRepo.Delete(id);
            this._unitOfWork.Save();
            return true;
        }
    }
}
