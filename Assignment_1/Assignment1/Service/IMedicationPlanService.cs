﻿using Assignment1.Models.Entities;
using Assignment1.Models.Entities.EntitiesMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Assignment1.Service
{
    public interface IMedicationPlanService
    {
        public bool AddPlan(MedicationPlan medicationPlan);
        public IEnumerable<MedicationPlan> GetPlan();
        public IEnumerable<MedicationPlan> GetPlanById(int id);
        public IEnumerable<MedicationPlanMapper> GetMedicationPlansByPatient(int id);
        public bool UpdatePlan(MedicationPlan medicationPlan);
        public bool DeletePlan(int id);
        
    }
}
