﻿using Assignment1.Models.Entities;
using Assignment1.Models.Entities.EntitiesMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Assignment1.Models.MessageReceive;

namespace Assignment1.Service
{
    public class Mapping : Profile
    {
        public Mapping()
        {
            CreateMap<MedicationPlan, MedicationPlanMapper>();
            CreateMap<MedicationStrategy, MedicationStrategyMapper>();
            CreateMap<Medication, MedicationMapper>();
            CreateMap<Patient, PatientMapper>();
            CreateMap<Activity, ActivityMapper>();
        }
    }
}
