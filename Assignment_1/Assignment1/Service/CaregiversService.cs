﻿using Assignment1.DAL;
using Assignment1.Models.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Assignment1.Service
{
    public class CaregiversService:ICaregiverService
    {
        private IUnitOfWork _unitOfWork;

        public CaregiversService(IUnitOfWork unitOfWork)
        {
            this._unitOfWork = unitOfWork;
        }

        public bool AddCaregiver(Caregiver caregiver)
        {
            this._unitOfWork.CaregiverRepo.Insert(caregiver);
            this._unitOfWork.Save();
            return false;
        }

        public IEnumerable<Caregiver> GetCaregivers()
        {
            Func<IQueryable<Caregiver>, IOrderedQueryable<Caregiver>> order = query => query.OrderBy(caregiver => caregiver.Id);
            return this._unitOfWork.CaregiverRepo.Get(orderBy: order);
        }
        public IEnumerable<Caregiver> GetCaregiversById(int id)
        {
            Expression<Func<Caregiver, bool>> query = caregiver => caregiver.Id == id;
            return this._unitOfWork.CaregiverRepo.Get(query);
        }

        public Caregiver GetCaregiverByUserId(string userId)
        {
            Expression<Func<Caregiver, bool>> query = caregiver => caregiver.UserId == userId;
            return this._unitOfWork.CaregiverRepo.Get(query).SingleOrDefault();
        }

        public bool UpdateCaregiver(Caregiver caregiver)
        {
            this._unitOfWork.CaregiverRepo.Update(caregiver);
            this._unitOfWork.Save();
            return true;
        }

        public bool DeleteCaregiver(int id)
        {
            this._unitOfWork.CaregiverRepo.Delete(id);
            this._unitOfWork.Save();
            return true;
        }
    }
}
