﻿using Assignment1.Models.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Assignment1.Service
{
    public interface IDoctorService
    {
        public bool AddDoctor(Doctor doctor);
        public IEnumerable<Doctor> GetDoctor();
        public Doctor GetDoctorByUserId(string userId);
      
    }
}
