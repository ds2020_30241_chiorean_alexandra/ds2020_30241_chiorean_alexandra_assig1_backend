﻿using Assignment1.Models.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Assignment1.Service
{
    public interface IMedicationService
    {
        public bool AddMedication(Medication drug);
        public IEnumerable<Medication> GetMedication();
        public IEnumerable<Medication> GetMedicationById(int id);
        public bool UpdateMedication(Medication drug);
        public bool DeleteMedication(int id);
       
    }
}
