﻿using Assignment1.Models.Entities;
using Assignment1.Models.Entities.EntitiesMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Assignment1.Service
{
    public interface IPatientService
    {
        public bool AddPatient(Patient patient);
        public IEnumerable<Patient> GetPatients();
        public Patient GetPatientsById(int id);
        public IEnumerable<Patient> GetPatientsByDoctor(int doctorId);
        public IEnumerable<PatientMapper> GetPatientsByCaregiver(int caregiverId);
        public Patient GetPatientByUserId(string userId);
        public bool UpdatePatient(Patient patient);
        public bool DeletePatient(int id);
        
    }
}
