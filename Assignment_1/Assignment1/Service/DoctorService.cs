﻿using Assignment1.DAL;
using Assignment1.Models.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Assignment1.Service
{
    public class DoctorService:IDoctorService
    {
        private IUnitOfWork _unitOfWork;

        public DoctorService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public bool AddDoctor(Doctor doctor)
        {
            _unitOfWork.DoctorRepo.Insert(doctor);
            _unitOfWork.Save();
            return true;
        }

        public IEnumerable<Doctor> GetDoctor()
        {
            return _unitOfWork.DoctorRepo.Get();
        }

        public Doctor GetDoctorByUserId(string userId)
        {
            Expression<Func<Doctor, bool>> query = doctor => doctor.UserId == userId;
            return _unitOfWork.DoctorRepo.Get(query, null, "User").SingleOrDefault();
        }
    }
}
