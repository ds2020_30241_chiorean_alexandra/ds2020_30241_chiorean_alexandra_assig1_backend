﻿using Assignment1.DAL;
using Assignment1.Models.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using AutoMapper;
using Assignment1.Models.Entities.EntitiesMapper;

namespace Assignment1.Service
{
    public class MedicationPlanService:IMedicationPlanService
    {
        private IUnitOfWork _unitOfWork;
        private IMapper _mapper;

        public MedicationPlanService(IMapper mapper, IUnitOfWork unitOfWork)
        {
            this._unitOfWork = unitOfWork;
            this._mapper = mapper;
        }

        public bool AddPlan(MedicationPlan medicationPlan)
        {
            this._unitOfWork.MedicationPlanRepo.Insert(medicationPlan);
            this._unitOfWork.Save();
            return true;
        }

        public IEnumerable<MedicationPlan> GetPlan()
        {
            Func<IQueryable<MedicationPlan>, IOrderedQueryable<MedicationPlan>> order = query => query.OrderBy(medPln => medPln.Id);
            return this._unitOfWork.MedicationPlanRepo.Get();
        }

        public IEnumerable<MedicationPlan> GetPlanById(int id)
        {
            Expression<Func<MedicationPlan, bool>> query = plan => plan.Id == id;
            return this._unitOfWork.MedicationPlanRepo.Get();

        }

        public IEnumerable<MedicationPlanMapper> GetMedicationPlansByPatient(int id)
        {
            Expression<Func<MedicationPlan, bool>> query = plan => plan.PatientId == id;
            var result = this._unitOfWork.MedicationPlanRepo.Get(query, null, "MedsList,MedsList.Medication");
            var return_maps = this._mapper.Map<IEnumerable<MedicationPlanMapper>>(result);
            return return_maps;
        }

        public bool UpdatePlan(MedicationPlan medicationPlan)
        {
            this._unitOfWork.MedicationPlanRepo.Update(medicationPlan);
            this._unitOfWork.Save();
            return true;
        }

        public bool DeletePlan(int id)
        {
            this._unitOfWork.MedicationPlanRepo.Delete(id);
            this._unitOfWork.Save();
            return true;
        }
    }
}
