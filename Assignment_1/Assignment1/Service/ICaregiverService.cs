﻿using Assignment1.Models.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Assignment1.Service
{
    public interface ICaregiverService
    {
        public bool AddCaregiver(Caregiver caregiver);

        public IEnumerable<Caregiver> GetCaregivers();
        public IEnumerable<Caregiver> GetCaregiversById(int id);


        public Caregiver GetCaregiverByUserId(string userId);

        public bool UpdateCaregiver(Caregiver caregiver);

        public bool DeleteCaregiver(int id);
    }
}
