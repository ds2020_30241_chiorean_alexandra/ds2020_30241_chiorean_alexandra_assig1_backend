﻿using Assignment1.DAL;
using Assignment1.Hubs;
using Assignment1.Hubs.Clients;
using Assignment1.Models.MessageReceive;
using Microsoft.AspNetCore.SignalR;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Newtonsoft.Json;
using Npgsql.TypeHandlers.FullTextSearchHandlers;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Assignment1.Service.MessageReceive
{
    public class Receiver : BackgroundService
    {
        private readonly string _hostname = "fox.rmq.cloudamqp.com";
        private readonly string _queueName = "ActivityQueue";
        private readonly string _username = "qinhaarc";
        private readonly string _password = "r_wdyHoH7HwqlJJKKJjiSiKNxHG9nu_a";
        private readonly IServiceScopeFactory _serviceScopeFactory;
        private readonly IHubContext<NotificationHub, INotificationClient> _notificationHub;
        private IConnection _connection;
        private IModel _channel;



        public Receiver(IServiceScopeFactory service, IHubContext<NotificationHub, INotificationClient> notificationHub)
        {
            this._serviceScopeFactory = service;
            this._notificationHub = notificationHub;
            var factory = new ConnectionFactory() { 
                HostName = _hostname, 
                UserName = _username, 
                Password = _password,
                VirtualHost = _username
            };
            //var factory = new ConnectionFactory();
            //factory.Uri = new System.Uri( "amqps://qinhaarc:r_wdyHoH7HwqlJJKKJjiSiKNxHG9nu_a@fox.rmq.cloudamqp.com/qinhaarc");
            _connection = factory.CreateConnection();
            _channel = _connection.CreateModel();
            _channel.QueueDeclare(queue: _queueName, durable: false, exclusive: false, autoDelete: false, arguments: null);


        }
        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            stoppingToken.ThrowIfCancellationRequested();
               
                  
            var consumer = new EventingBasicConsumer(_channel);
            Console.WriteLine(" [*] Waiting for messages.");

            consumer.Received += async (model, ea) =>
            {
                using (var scope = _serviceScopeFactory.CreateScope())
                {
                    var _unitOfWork = scope.ServiceProvider.GetRequiredService<IUnitOfWork>();
                    var body = ea.Body.ToArray();
                    var message = Encoding.UTF8.GetString(body);
                    var activityReceived = JsonConvert.DeserializeObject<ActivityMapper>(message);
                    Console.WriteLine(" [x] Received {0}", message);
                    var activity = new Activity(activityReceived.PatientId, activityReceived.StartDate, activityReceived.EndDate, activityReceived.Name);
                    _unitOfWork.ActivityRepo.Insert(activity);
                    _unitOfWork.Save();
                    if (CheckRules(activity))
                    {
                        TimeSpan ts = activity.EndDate - activity.StartDate;
                        var patient = _unitOfWork.PatientRepo.GetByID(activity.PatientId);
                        var notification = new Notification(patient.CaregiverId, "Patient " + patient.Name + " needs help. Anomaly: doing " + activity.Name + " for " + ts);
                        await _notificationHub.Clients.All.ReceiveMessage(notification);
                        Console.WriteLine(notification);
                    }
                    Thread.Sleep(1000);
                }
            };
            _channel.BasicConsume(queue: _queueName, autoAck: true, consumer: consumer);
                
            Console.WriteLine("Here");
            await Task.CompletedTask;
        }
        public bool CheckRules(Activity activity)
        {
            TimeSpan ts = activity.EndDate - activity.StartDate;
            if (activity.Name == "Sleeping" && ts.Hours > 7)
            {
                return true;
            }
            if (activity.Name == "Leaving" && ts.Hours > 5)
            {
                return true;
            }
            if ((activity.Name == "Showering" || activity.Name== "Toileting" || activity.Name == "Grooming") && ts.Minutes > 30)
            {
                return true;
            }
            return false;
        }
    }
}
