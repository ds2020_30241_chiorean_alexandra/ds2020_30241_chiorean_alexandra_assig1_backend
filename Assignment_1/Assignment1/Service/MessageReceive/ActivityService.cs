﻿using Assignment1.DAL;
using Assignment1.Models.MessageReceive;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Assignment1.Service.MessageReceive
{
    public class ActivityService:IActivityService
    {
        private IUnitOfWork _unitOfWork;
        public ActivityService(IUnitOfWork unitOfWork)
        {
            this._unitOfWork = unitOfWork;
        }
        public bool AddActivity(Activity activity)
        {
            _unitOfWork.ActivityRepo.Insert(activity);
            _unitOfWork.Save();
            return true;
        }
    }
}
