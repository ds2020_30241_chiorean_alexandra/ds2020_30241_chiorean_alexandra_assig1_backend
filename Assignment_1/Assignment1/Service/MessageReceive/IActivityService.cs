﻿using Assignment1.Models.MessageReceive;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Assignment1.Service.MessageReceive
{
    public interface IActivityService
    {
        public bool AddActivity(Activity activity);
    }
}
