﻿using Assignment1.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Assignment1.DAL;
using Assignment1.Models.Entities;
using System.Linq.Expressions;
using AutoMapper;
using Assignment1.Models.Entities.EntitiesMapper;

namespace Assignment1.Service
{
    public class PatientService:IPatientService
    {
        private IUnitOfWork _unitOfWork;
        private IMapper _mapper;

        public PatientService(IMapper _mapper, IUnitOfWork unitOfWork)
        {
           this._unitOfWork = unitOfWork;
            this._mapper = _mapper;
        }

        public bool AddPatient(Patient patient)
        {
            this._unitOfWork.PatientRepo.Insert(patient);
            this._unitOfWork.Save();
            return false;
        }

        public IEnumerable<Patient> GetPatients()
        {
            Func<IQueryable<Patient>, IOrderedQueryable<Patient>> order = query => query.OrderBy(patient => patient.Id);
            return this._unitOfWork.PatientRepo.Get(orderBy:order);

        }
        public Patient GetPatientsById(int id)
        {
            Expression<Func<Patient, bool>> query = patient => patient.Id == id;
            return this._unitOfWork.PatientRepo.Get(query, null, "User").SingleOrDefault();
        }

        public IEnumerable<Patient> GetPatientsByDoctor(int doctorId)
        {
            Expression<Func<Patient, bool>> query = patient => patient.DoctorId == doctorId;
            return this._unitOfWork.PatientRepo.Get(query);
        }

        public IEnumerable<PatientMapper> GetPatientsByCaregiver(int caregiverId)
        {
            Expression<Func<Patient, bool>> query = patient => patient.CaregiverId == caregiverId;
            var queryResult = this._unitOfWork.PatientRepo.Get(query, null, "MedicationPlans,MedicationPlans.MedsList,MedicationPlans.MedsList.Medication");
            return this._mapper.Map<IEnumerable<PatientMapper>>(queryResult);
        }

        public Patient GetPatientByUserId(string userId)
        {
            Expression<Func<Patient, bool>> query = patient => patient.UserId == userId;
            return this._unitOfWork.PatientRepo.Get(query).SingleOrDefault();
        }

        public bool UpdatePatient(Patient patient)
        {
            this._unitOfWork.PatientRepo.Update(patient);
            this._unitOfWork.Save();
            return true;
        }

        public bool DeletePatient(int id)
        {
            this._unitOfWork.PatientRepo.Delete(id);
            this._unitOfWork.Save();
            return true;
        }
            

    }
}
