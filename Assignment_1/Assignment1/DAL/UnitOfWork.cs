﻿using Assignment1.DAL.Repositories;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Assignment1.DAL
{
    public class UnitOfWork : IUnitOfWork
    {
        private ApplicationContext _context;
        private DoctorRepository _doctorRepo;
        private PatientRepository _patientRepo;
        private MedicationRepository _medicationRepo;
        private CaregiverRepository _caregiverRepo;
        private MedicationPlanRepository _medPlanRepo;
        private ActivityRepository _activityRepo;
        private MedicationIntakeRepository _medicationIntakeRepo;

        public UnitOfWork(ApplicationContext context)
        {   
            _context = context;
        }

        public PatientRepository PatientRepo
        {
            get
            {

                if (this._patientRepo == null)
                {
                    this._patientRepo = new PatientRepository(this._context);
                }
                return _patientRepo;
            }
        }

        public DoctorRepository DoctorRepo
        {
            get
            {

                if (this._doctorRepo == null)
                {
                    this._doctorRepo = new DoctorRepository(this._context);
                }
                return _doctorRepo;
            }
        }

        public MedicationRepository MedicationRepo
        {
            get
            {

                if (this._medicationRepo == null)
                {
                    this._medicationRepo = new MedicationRepository(this._context);
                }
                return _medicationRepo;
            }
        }

        public CaregiverRepository CaregiverRepo
        {
            get
            {

                if (this._caregiverRepo == null)
                {
                    this._caregiverRepo = new CaregiverRepository(this._context);
                }
                return _caregiverRepo;
            }
        }

        public MedicationPlanRepository MedicationPlanRepo
        {
            get
            {

                if (this._medPlanRepo == null)
                {
                    this._medPlanRepo = new MedicationPlanRepository(this._context);
                }
                return _medPlanRepo;
            }
        }

        public ActivityRepository ActivityRepo
        {
            get
            {

                if (this._activityRepo == null)
                {
                    this._activityRepo = new ActivityRepository(this._context);
                }
                return _activityRepo;
            }
        }

        public MedicationIntakeRepository MedicationIntakeRepo
        {
            get
            {

                if (this._medicationIntakeRepo == null)
                {
                    this._medicationIntakeRepo = new MedicationIntakeRepository(this._context);
                }
                return _medicationIntakeRepo;
            }
        }

        public void Save()
        {
            this._context.SaveChanges();
        }

        

    }
}
