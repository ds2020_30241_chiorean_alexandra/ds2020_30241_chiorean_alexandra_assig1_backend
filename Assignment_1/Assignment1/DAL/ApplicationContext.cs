﻿using Assignment1.Models.Entities;
using Assignment1.Models.Entities.Authentication;
using Assignment1.Models.MessageReceive;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Assignment1.DAL
{
    public class ApplicationContext:IdentityDbContext<UserAccount, IdentityRole, string>
    {
        public ApplicationContext(DbContextOptions<ApplicationContext> options)
            : base(options)
        {

        }
        public DbSet<Doctor> Doctors { get; set;}
        public DbSet<Patient> Pacients { get; set; }
        public DbSet<Caregiver> Caregivers { get; set; }
        public DbSet<Medication> Medications { get; set; }
        public DbSet<MedicationPlan> MedicationPlans { get; set; }
        public DbSet<MedicalRecord> Medicalrecord { get; set; }
        public DbSet<MedicationStrategy> MedicationStrategy { get; set; }
        public DbSet<Activity> Activity { get; set; }
        public DbSet<MedicationIntake> MedicationIntake { get; set; }
    }
}
