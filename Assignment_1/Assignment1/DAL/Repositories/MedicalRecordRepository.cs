﻿using Assignment1.Models.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Assignment1.DAL.Repositories
{
    public class MedicalRecordRepository : GenericRepository<MedicalRecord>
    {
        public MedicalRecordRepository(ApplicationContext context) : base(context)
        {
            this.context = context;
        }
    }
}
