﻿using Assignment1.Models.MessageReceive;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Assignment1.DAL.Repositories
{
    public class ActivityRepository :GenericRepository<Activity>
    {
        public ActivityRepository(ApplicationContext context) : base(context)
        {
            this.context = context;
        }
    }
}
