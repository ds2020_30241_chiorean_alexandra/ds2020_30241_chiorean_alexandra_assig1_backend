﻿using Assignment1.DAL.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Assignment1.DAL
{
    public interface IUnitOfWork
    {
        public PatientRepository PatientRepo { get; }

        public DoctorRepository DoctorRepo { get; }

        public MedicationRepository MedicationRepo { get; }

        public CaregiverRepository CaregiverRepo { get; }

        public MedicationPlanRepository MedicationPlanRepo { get; }

        public ActivityRepository ActivityRepo { get; }
        public MedicationIntakeRepository MedicationIntakeRepo { get; }

        public void Save();
    }
}
