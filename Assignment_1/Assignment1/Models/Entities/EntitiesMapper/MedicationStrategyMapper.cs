﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Assignment1.Models.Entities.EntitiesMapper
{
    public class MedicationStrategyMapper
    {
        public MedicationMapper Medication { get; set; }
        public string InTake { get; set; }
    }
}
