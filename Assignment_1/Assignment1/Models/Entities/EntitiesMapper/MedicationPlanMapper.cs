﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Assignment1.Models.Entities.EntitiesMapper
{
    public class MedicationPlanMapper
    {
        public IEnumerable<MedicationStrategyMapper> MedsList { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
    }
}
