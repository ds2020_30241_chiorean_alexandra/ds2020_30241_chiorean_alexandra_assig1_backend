﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Assignment1.Models.Entities.EntitiesMapper
{
    public class MedicationMapper
    {
        public string Name { get; set; }
        public string SideEffects { get; set; }
        public float Dosage { get; set; }
    }
}
