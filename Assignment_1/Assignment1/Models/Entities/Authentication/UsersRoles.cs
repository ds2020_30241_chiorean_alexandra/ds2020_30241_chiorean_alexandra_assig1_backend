﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Assignment1.Models.Entities.Authentication
{
    public class UsersRoles:IdentityRole<string>
    {
        public const string Admin = "Admin";
        public const string Doctor = "Doctor";
        public const string Caregiver = "Caregiver";
        public const string Patient = "Patient";
    }
}
