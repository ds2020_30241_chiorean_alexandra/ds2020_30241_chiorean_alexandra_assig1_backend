﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Assignment1.Models.Entities.Authentication
{
    public class RegisterModel
    {
       public string Username { get; set; }

       public string Email { get; set; }

       public string Password { get; set; }

       public string Role { get; set; }
    }
}
