﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Assignment1.Models.Entities
{
    public class MedicalRecord
    {
        [Key]
        public int ID { get; set; }
        public string description { get; set; }
        public DateTime date { get; set; }

    }
}
