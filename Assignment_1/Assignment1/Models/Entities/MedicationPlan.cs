﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Assignment1.Models.Entities
{
    public class MedicationPlan
    {
        [Key]
        public int Id { get; set; }
        public IEnumerable<MedicationStrategy> MedsList { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public int PatientId { get; set; }
        public Patient Patient { get; set; }
    }
}
