﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Assignment1.Models.Entities
{
    public class MedicationIntake
    {
        [Key]
        public Guid Id { get; set; }
        public int PatientId { get; set; }
        public Patient Patient { get; set; }
        public int MedicationId { get; set; }
        public Medication Medication { get; set; }
        public DateTime intakeTime { get; set; }
        public string Status { get; set; }
        
    }
}
