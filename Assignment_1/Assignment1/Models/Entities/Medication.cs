﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

/*
 * ID, name, list of side effects, dosage
 */
namespace Assignment1.Models.Entities
{
    public class Medication
    {
        [Key]
        public int Id { get; set; }
        public string Name { get; set; }
        public string SideEffects { get; set; }
        public float Dosage { get; set; }
    }
}
