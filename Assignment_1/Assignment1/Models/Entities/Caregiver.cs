﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

/*
 ID, name, birth date, gender, address, list of patients taken care of
*/
namespace Assignment1.Models.Entities
{
    public class Caregiver
    {
        [Key]
        public int Id { get; set; }
        public string Name { get; set; }
        public DateTime BirthDate { get; set; }
        public string Gender { get; set; }
        public string Address { get; set; }
        public string UserId { get; set; }
        public UserAccount User { get; set; }
        public ICollection<Patient> PatientsInCare { get; set; }

        public override bool Equals(Object obj)
        {
            //Check for null and compare run-time types.
            if ((obj == null) || !this.GetType().Equals(obj.GetType()))
            {
                return false;
            }
            else
            {
                Caregiver p = (Caregiver)obj;
                return (Name == p.Name) && (Address == p.Address)
                    && (Gender == p.Gender) && (UserId == p.UserId)
                    && (BirthDate == p.BirthDate);
            }
        }

    }
}
