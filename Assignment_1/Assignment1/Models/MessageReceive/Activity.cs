﻿using Assignment1.Models.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Assignment1.Models.MessageReceive
{
    public class Activity
    {
        [Key]
        public int Id { get; set; }
        public Patient Patient { get; set; }
        public int PatientId { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public string Name { get; set; }
        public Activity(Patient Patient, int PatientId, DateTime StartDate, DateTime EndDate, string Name)
        {
            this.PatientId = PatientId;
            this.StartDate = StartDate;
            this.EndDate = EndDate;
            this.Name = Name;
            this.Patient = Patient;
        }
        public Activity(int PatientId, DateTime StartDate, DateTime EndDate, string Name)
        {
            this.PatientId = PatientId;
            this.StartDate = StartDate;
            this.EndDate = EndDate;
            this.Name = Name;
            this.Patient = null;
        }
        
    }
}
