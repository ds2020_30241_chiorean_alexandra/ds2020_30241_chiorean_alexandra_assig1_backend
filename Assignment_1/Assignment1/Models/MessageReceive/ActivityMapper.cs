﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Assignment1.Models.MessageReceive
{
    public class ActivityMapper
    {
        public int PatientId { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public string Name { get; set; }

        public ActivityMapper(int id, DateTime startDate, DateTime endDate, string name)
        {
            this.PatientId = id;
            this.StartDate = startDate;
            this.EndDate = endDate;
            this.Name = name;
        }

        public override string ToString()
        {
            return PatientId + " " + Name + " " + StartDate;
        }
    }
}
