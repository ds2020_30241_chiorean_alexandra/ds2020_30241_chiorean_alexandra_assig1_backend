﻿using Assignment1.Models.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Assignment1.Hubs
{
    public class Notification
    { 
        public int User { get; set; }

        public string Message { get; set; }

        public Notification(int user, string message)
        {
            this.User = user;
            this.Message = message;
        }
    }
}
