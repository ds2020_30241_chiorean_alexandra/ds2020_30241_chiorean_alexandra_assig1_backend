﻿using Assignment1.Hubs.Clients;
using Microsoft.AspNetCore.SignalR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Assignment1.Hubs
{
    public class NotificationHub : Hub<INotificationClient>
    {
        public async Task SendMessage(Notification notification)
        {
            await Clients.All.ReceiveMessage( notification);
        }
    }
}
