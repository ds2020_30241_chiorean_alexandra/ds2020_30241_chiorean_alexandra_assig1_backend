﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Assignment1.Migrations
{
    public partial class modify_fk_patient : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_MedicationStrategy_MedicationPlans_MedicationPlanId",
                table: "MedicationStrategy");

            migrationBuilder.RenameColumn(
                name: "MedicationPlanId",
                table: "MedicationStrategy",
                newName: "medicationPlanId");

            migrationBuilder.RenameIndex(
                name: "IX_MedicationStrategy_MedicationPlanId",
                table: "MedicationStrategy",
                newName: "IX_MedicationStrategy_medicationPlanId");

            migrationBuilder.AlterColumn<int>(
                name: "medicationPlanId",
                table: "MedicationStrategy",
                nullable: false,
                oldClrType: typeof(int),
                oldType: "integer",
                oldNullable: true);

            migrationBuilder.AddForeignKey(
                name: "FK_MedicationStrategy_MedicationPlans_medicationPlanId",
                table: "MedicationStrategy",
                column: "medicationPlanId",
                principalTable: "MedicationPlans",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_MedicationStrategy_MedicationPlans_medicationPlanId",
                table: "MedicationStrategy");

            migrationBuilder.RenameColumn(
                name: "medicationPlanId",
                table: "MedicationStrategy",
                newName: "MedicationPlanId");

            migrationBuilder.RenameIndex(
                name: "IX_MedicationStrategy_medicationPlanId",
                table: "MedicationStrategy",
                newName: "IX_MedicationStrategy_MedicationPlanId");

            migrationBuilder.AlterColumn<int>(
                name: "MedicationPlanId",
                table: "MedicationStrategy",
                type: "integer",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AddForeignKey(
                name: "FK_MedicationStrategy_MedicationPlans_MedicationPlanId",
                table: "MedicationStrategy",
                column: "MedicationPlanId",
                principalTable: "MedicationPlans",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
