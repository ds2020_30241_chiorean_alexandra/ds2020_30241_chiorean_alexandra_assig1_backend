﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Assignment1.Migrations
{
    public partial class AddUserToCaregiver : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "MedicalRecord",
                table: "Pacients",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "UserId",
                table: "Caregivers",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Caregivers_UserId",
                table: "Caregivers",
                column: "UserId");

            migrationBuilder.AddForeignKey(
                name: "FK_Caregivers_AspNetUsers_UserId",
                table: "Caregivers",
                column: "UserId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Caregivers_AspNetUsers_UserId",
                table: "Caregivers");

            migrationBuilder.DropIndex(
                name: "IX_Caregivers_UserId",
                table: "Caregivers");

            migrationBuilder.DropColumn(
                name: "MedicalRecord",
                table: "Pacients");

            migrationBuilder.DropColumn(
                name: "UserId",
                table: "Caregivers");
        }
    }
}
