﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Assignment1.Migrations
{
    public partial class Check : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Gener",
                table: "Caregivers");

            migrationBuilder.AddColumn<string>(
                name: "Gender",
                table: "Caregivers",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Gender",
                table: "Caregivers");

            migrationBuilder.AddColumn<char>(
                name: "Gener",
                table: "Caregivers",
                type: "character(1)",
                nullable: false,
                defaultValue: ' ');
        }
    }
}
