﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Assignment1.Migrations
{
    public partial class AddIds : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_MedicationPlans_Pacients_PatientId",
                table: "MedicationPlans");

            migrationBuilder.DropForeignKey(
                name: "FK_MedicationStrategy_MedicationPlans_MedicationPlanID",
                table: "MedicationStrategy");

            migrationBuilder.DropForeignKey(
                name: "FK_MedicationStrategy_Medications_medsId",
                table: "MedicationStrategy");

            migrationBuilder.DropIndex(
                name: "IX_MedicationStrategy_medsId",
                table: "MedicationStrategy");

            migrationBuilder.DropColumn(
                name: "medsId",
                table: "MedicationStrategy");

            migrationBuilder.RenameColumn(
                name: "MedicationPlanID",
                table: "MedicationStrategy",
                newName: "MedicationPlanId");

            migrationBuilder.RenameColumn(
                name: "ID",
                table: "MedicationStrategy",
                newName: "Id");

            migrationBuilder.RenameIndex(
                name: "IX_MedicationStrategy_MedicationPlanID",
                table: "MedicationStrategy",
                newName: "IX_MedicationStrategy_MedicationPlanId");

            migrationBuilder.RenameColumn(
                name: "PatientId",
                table: "MedicationPlans",
                newName: "patientId");

            migrationBuilder.RenameColumn(
                name: "ID",
                table: "MedicationPlans",
                newName: "Id");

            migrationBuilder.RenameIndex(
                name: "IX_MedicationPlans_PatientId",
                table: "MedicationPlans",
                newName: "IX_MedicationPlans_patientId");

            migrationBuilder.AddColumn<int>(
                name: "medicationId",
                table: "MedicationStrategy",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AlterColumn<int>(
                name: "patientId",
                table: "MedicationPlans",
                nullable: false,
                oldClrType: typeof(int),
                oldType: "integer",
                oldNullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_MedicationStrategy_medicationId",
                table: "MedicationStrategy",
                column: "medicationId");

            migrationBuilder.AddForeignKey(
                name: "FK_MedicationPlans_Pacients_patientId",
                table: "MedicationPlans",
                column: "patientId",
                principalTable: "Pacients",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_MedicationStrategy_MedicationPlans_MedicationPlanId",
                table: "MedicationStrategy",
                column: "MedicationPlanId",
                principalTable: "MedicationPlans",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_MedicationStrategy_Medications_medicationId",
                table: "MedicationStrategy",
                column: "medicationId",
                principalTable: "Medications",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_MedicationPlans_Pacients_patientId",
                table: "MedicationPlans");

            migrationBuilder.DropForeignKey(
                name: "FK_MedicationStrategy_MedicationPlans_MedicationPlanId",
                table: "MedicationStrategy");

            migrationBuilder.DropForeignKey(
                name: "FK_MedicationStrategy_Medications_medicationId",
                table: "MedicationStrategy");

            migrationBuilder.DropIndex(
                name: "IX_MedicationStrategy_medicationId",
                table: "MedicationStrategy");

            migrationBuilder.DropColumn(
                name: "medicationId",
                table: "MedicationStrategy");

            migrationBuilder.RenameColumn(
                name: "MedicationPlanId",
                table: "MedicationStrategy",
                newName: "MedicationPlanID");

            migrationBuilder.RenameColumn(
                name: "Id",
                table: "MedicationStrategy",
                newName: "ID");

            migrationBuilder.RenameIndex(
                name: "IX_MedicationStrategy_MedicationPlanId",
                table: "MedicationStrategy",
                newName: "IX_MedicationStrategy_MedicationPlanID");

            migrationBuilder.RenameColumn(
                name: "patientId",
                table: "MedicationPlans",
                newName: "PatientId");

            migrationBuilder.RenameColumn(
                name: "Id",
                table: "MedicationPlans",
                newName: "ID");

            migrationBuilder.RenameIndex(
                name: "IX_MedicationPlans_patientId",
                table: "MedicationPlans",
                newName: "IX_MedicationPlans_PatientId");

            migrationBuilder.AddColumn<int>(
                name: "medsId",
                table: "MedicationStrategy",
                type: "integer",
                nullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "PatientId",
                table: "MedicationPlans",
                type: "integer",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.CreateIndex(
                name: "IX_MedicationStrategy_medsId",
                table: "MedicationStrategy",
                column: "medsId");

            migrationBuilder.AddForeignKey(
                name: "FK_MedicationPlans_Pacients_PatientId",
                table: "MedicationPlans",
                column: "PatientId",
                principalTable: "Pacients",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_MedicationStrategy_MedicationPlans_MedicationPlanID",
                table: "MedicationStrategy",
                column: "MedicationPlanID",
                principalTable: "MedicationPlans",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_MedicationStrategy_Medications_medsId",
                table: "MedicationStrategy",
                column: "medsId",
                principalTable: "Medications",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
