using Assignment1.DAL;
using Assignment1.Models.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using Xunit;

namespace Test
{
    public class UnitTest1
    {
        [Fact]
        public void Test1()
        {
            var options = new DbContextOptionsBuilder<ApplicationContext>()
                .UseInMemoryDatabase(databaseName: "MedicalPlatform")
                .Options;
            var context = new ApplicationContext(options);
            Seed(context);
            var uow = new UnitOfWork(context);
            var result = uow.CaregiverRepo.Get();
            var date1 = new DateTime(2008, 3, 1, 7, 0, 0);
            Assert.Equal(2, result.Count());
            Assert.Equal(result.ElementAt(0), new Caregiver { Name = "Caregiver1", Address = "Address1", Gender = "Female", BirthDate = date1 });
        }


        public void Seed(ApplicationContext context)
        {
            var date1 = new DateTime(2008, 3, 1, 7, 0, 0);
            var caregivers = new[]
            {
                new Caregiver{Name = "Caregiver1", Address="Address1", Gender="Female", BirthDate=date1},
                new Caregiver{Name = "Caregiver2", Address="Address2", Gender="Female", BirthDate=date1},
            };
            context.Caregivers.AddRange(caregivers);
            context.SaveChanges();
        }
    }

}
